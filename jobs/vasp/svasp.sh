#!/bin/bash

# Account and partition
# ---------------------
#SBATCH --account=ens
#SBATCH --partition=compute

# Email notifications
# -------------------
##SBATCH --mail-type=ALL
##SBATCH --mail-user=prenom.nom@univ-pau.fr

# Time
# ----
#SBATCH --time=0-04:00:00

# MPI ressources
# ------------------
#SBATCH --ntasks=<specify here the number of cores>
#SBATCH --cpus-per-task=1

# Memory per core (MB) 
# --------------------
#SBATCH --mem-per-cpu=1000

# Module loading
# --------------
module purge
module load vasp/5.3.5
module list

# Commands
# --------
echo "vasp : `which vasp`"
echo "------------------------------------------------------------"
echo "SLURM_JOB_NAME       =" $SLURM_JOB_NAME
echo "SLURM_JOBID          =" $SLURM_JOBID
echo "SLURM_NODELIST       = "$SLURM_NODELIST
echo ""
echo "SLURM_NNODES         =" $SLURM_NNODES
echo "SLURM_NTASKS         =" $SLURM_NTASKS
echo "SLURM_TASKS_PER_NODE =" $SLURM_TASKS_PER_NODE
echo "SLURM_CPUS_PER_TASK  =" $SLURM_CPUS_PER_TASK
echo "SLURM_NPROCS         =" $SLURM_NPROCS
echo "------------------------------------------------------------"

# Execution
# ---------
time mpiexec vasp
