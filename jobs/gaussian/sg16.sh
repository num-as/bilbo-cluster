#!/bin/bash
#
#### GAUSSIAN 16 JOB #############
#
#  Usage: sbatch sg16.sh inputfile
#
##################################
#
#
#
########################################### BEGIN SLURM OPTIONS  ##################################
#
#
# JOB AND OUTPUT FILE NAMES. Uncomment if needed.
#   If not specified, the default output file name will be slurm-NNNN.out, 
#    where NNNN is the Slurm job id.
##SBATCH --job-name=NAME-TEST
##SBATCH --output=output_slurm.out
#
# MAIL NOTIFICATIONS (at job start, step, end or failure). Uncomment if needed.
##SBATCH --mail-type=ALL
##SBATCH --mail-user=firstname.lastname@univ-pau.fr
#
# EXECUTION IN THE CURRENT DIRECTORY
#SBATCH --workdir=.
#
# PARTITION
# The following command provides the state of available partitions:
#   sinfo
#SBATCH --partition=compute
#
# ACCOUNT
# The following command provides the list of accounts you can use:
#   sacctmgr list user withassoc name=your_username format=user,account,defaultaccount
#SBATCH --account=ens
#
# JOB MAXIMAL WALLTIME. Format: D-H:M:S
#SBATCH --time=0-0:10:00
#
# CORES NUMBER 
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=<specify here the number of cores>
#
# MEMORY PER CORE (MB)
#SBATCH --mem-per-cpu=1000
#
#
########################################### END SLURM OPTIONS ##################################
#
#
#
######################## BEGIN UNIX COMMANDS  #########################

# MODULES
module purge
module load gaussian/g16-b01

# COMMANDS
. $g16root/g16/bsd/g16.profile
export GAUSS_SCRDIR=$SCRATCHDIR
FICIN=$(echo ${1%.*})
echo '===============================================' >  "$FICIN"_g16.out
echo "Execution on node     : `uname -n`"              >> "$FICIN"_g16.out
echo -e "Job id = $SLURM_JOB_ID"                       >> "$FICIN"_g16.out
echo "Temporary files on: " $GAUSS_SCRDIR              >> "$FICIN"_g16.out
echo '===============================================' >> "$FICIN"_g16.out
echo "Begin job execution: `date    `"                 >> "$FICIN"_g16.out
time g16 < $1                                          >> "$FICIN"_g16.out 2>&1
echo "End job execution: `date    `"                   >> "$FICIN"_g16.out

######################### END UNIX COMMANDS #########################
