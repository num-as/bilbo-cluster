#!/bin/bash
#
#### CRYSTAL 14 PARALLEL JOB (DISTRIBUTED MEMORY WITH OPEN MPI) ####
#
#  Usage: sbatch scrystal.sh input [input2]
#    The input file names are "input.d12" and "input2.{f9,...}"
#    (input and input2 are the extension-free names)
#
####################################################################
#
#
#
########################################### BEGIN SLURM OPTIONS  ##################################
#
#
# JOB AND OUTPUT FILE NAMES. Uncomment if needed.
#   If not specified, the default output file name will be slurm-NNNN.out, 
#    where NNNN is the Slurm job id.
##SBATCH --job-name=NAME-TEST
##SBATCH --output=output_slurm.out
#
# MAIL NOTIFICATIONS (at job start, step, end or failure). Uncomment if needed.
##SBATCH --mail-type=ALL
##SBATCH --mail-user=firstname.lastname@univ-pau.fr
#
# EXECUTION IN THE CURRENT DIRECTORY
#SBATCH --workdir=.
#
# PARTITION
# The following command provides the state of available partitions:
#   sinfo
#SBATCH --partition=compute
#
# ACCOUNT
# The following command provides the list of accounts you can use:
#   sacctmgr list user withassoc name=your_username format=user,account,defaultaccount
#SBATCH --account=ens
#
# JOB MAXIMAL WALLTIME. Format: D-H:M:S
#SBATCH --time=0-0:10:00
#
# CORES NUMBER 
#SBATCH --ntasks=<specify here the number of cores>
#SBATCH --cpus-per-task=1
#
# MEMORY PER CORE (MB)
#SBATCH --mem-per-cpu=1000
#
#
########################################### END SLURM OPTIONS ##################################
#
#
#
######################## BEGIN UNIX COMMANDS  #########################

# MODULES
module purge
module load crystal/14

# COMMANDS
# runmpi14 usage:
#  runmpi14 input [input2]
#    The input file names are "input.d12" and "input2.{f9,...}"
#    (input and input2 are the extension-free names)
time runmpi14 $1

######################### END UNIX COMMANDS #########################
